# -*- coding: utf-8 -*-
import json
from elasticsearch import Elasticsearch
import logging
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class PhonecollectorPipeline(object):
    def open_spider(self, spider):
        self.file = open('hc.json', 'w')
        self.es = Elasticsearch([{'host': 'localhost', 'port': 9200}])

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        line = json.dumps(dict(item)) + "\n"
        self.file.write(line + ",")
        return item
